function dataout = fitacmtf(data,f,varargin)

%% Input argument parsing

% TODO: 
% - Test sequential loop execution (non-parallel mode)
if strcmp(data,'options')
    options=defaultoptions;
    if strcmp(data,'options')
        dataout=options;
        return
    end
elseif isstruct(data)&&nargin==2
    options=defaultoptions;
elseif isstruct(data)&&nargin==3
    options=varargin{1};
end
starts    = options.starts;
convgcrit = options.convgcrit;
maxit     = options.maxit;
betasp    = options.beta;
nnflag    = options.nonnegativity;
% Temp (not yet implemented functionality)
constraints=[];
%% Initialize a few things
exmode=parorseq;
%% Set up the ACMTF model (parameters)

ncgopt = ncg('defaults');
ncgopt.Display ='final';
ncgopt.MaxFuncEvals = 100000;
ncgopt.MaxIters     = maxit;
ncgopt.StopTol      = convgcrit;
ncgopt.RelFuncTol   = convgcrit;
alg='lbfgsb';

numstarts=starts*numel(f);
factorarray=reshape(repmat(f,starts,1),numstarts,1);




%% Create dataset structure z
z=xy2z(data.t.data,data.m.data);

if ~cmtf_check(z)
    error('Dataset dimensions are not consistent.')
end
data.z=z;

%% Welcome messages

disp('  ')
disp('-----')
disp(' fitacmtf.m - Fit ACMTF model multiple times')
disp('-----')
switch exmode
    case 'sequential'
        disp(['Parallelization:                 ','no'])
    case 'parallel'
        disp(['Parallelization:                 ','yes'])
end
disp(['Models with components:          [',num2str(f),']'])
disp(['Number of random starts:         ',num2str(starts)])
disp(['Convergence criterion:           ',num2str(convgcrit)])
disp(['Maximum number of iterations:    ',num2str(maxit)])
disp(['Constraint:                      ',constraints])
disp('-----')
disp('  ')



%% Run the models
switch exmode
    case 'sequential'
        Model    = cell(numstarts,1);            % Allocate Model cell
        Iter     = cell(numstarts,1);            % Allocate cell for #of interations
        Err      = cell(numstarts,1);            % Allocate cell for modeling error
        fweights = cell(numstarts,1);            % Allocate cell for factor weights
        for j=1:numstarts
            [Zhat,~,stats]=acmtf_opt(z,factorarray(j),'init','random','alg_options',ncgopt, 'beta', betasp,'flag_nn',nnflag, 'alg',alg);
            Model{completedIdx,1} = rcvec([Zhat{1}.U(1:3);Zhat{2}.U(2)],'row');
            Iter{completedIdx,1} = stats.info_lbfgsb.iterations;
            Err{completedIdx,1} = [stats.F stats.info_lbfgsb.err(end,:)];
            fweights{completedIdx,1}=[Zhat{1}.lambda Zhat{2}.lambda];

        end
    case 'parallel'
        modout(1:numstarts) = parallel.FevalFuture; % Preallocate futures
        for j=1:numstarts
            modout(j)=parfeval(@acmtf_opt,3,z,factorarray(j),'init','random','alg_options',options, 'beta', betasp,'flag_nn',nnflag, 'alg',alg);
        end
        [Model,Iter,Err,fweights,ttc] = trackprogress(modout,factorarray,data);
end
%% Monitor modeling & analyze output

dataout = analyzeresults(data,f,factorarray,starts,maxit,convgcrit,constraints,{Model,Iter,Err,fweights,ttc});
dataout.z=z;
dataout.lastmodified=string(datetime);
dataout.history{end+1,1}=strcat(string(datetime),": Fitted ACMTF models with ",num2str(f)," components. (",num2str(starts)," models each).");

end % End fitacmtf.m




%% Misc functions used only in fitacmtf.m
function options=defaultoptions
options.starts=2;                   % No. of times a ACMTF model is initialized
options.convgcrit=1e-8;             % Convergence criterion for the ACMTF model
options.maxit=2.5e4;                  % Maximum number of iterations before fitting is stopped.
options.beta=[0 0];                 % Sparsity parameter on the weights of rank-one components of the data sets
options.nonnegativity=[1 1 1 1];    % Nonnegativity on factor matrices?
options.init='random';
end

function funmode=parorseq
test=ver;
funmode='sequential';
if any(contains({test.Name},'Parallel'))
    funmode='parallel';
    try
        warning off
        try
            poolsize=feature('NumCores');
            p = gcp('nocreate'); % If no pool, do not create new one.
            if isempty(p)
                parpool('local',poolsize);
            elseif p.NumWorkers~=poolsize
                disp('Found existing parpool with wrong number of workers.')
                disp(['Will now create pool with ',num2str(poolsize),' Workers.'])
                delete(p);
                parpool('local',poolsize);
            else
                disp(['Existing parallel pool of ',num2str(p.NumWorkers),' worker found and used...'])
            end
        catch ME
            rethrow(ME)
        end
        warning on
    catch
        funmode='sequential';
    end
end
end


function [Model,Iter,Err,fweights,ttc] = trackprogress(output,factorarray,data)
% Monitor parfeval progress supervision (c) Urban W�nsch, 2016-2019
% output      -> futures and their output
% factorarray -> array containing starts*f factor information
% data        -> data structure containing all possibly needed info for support

nmodels=numel(factorarray);
% Pre-allocation of modeling outputs
Model    = cell(nmodels,1);            % Allocate Model cell
Iter     = cell(nmodels,1);            % Allocate cell for #of interations
Err      = cell(nmodels,1);            % Allocate cell for modeling error
fweights = cell(nmodels,1);            % Allocate cell for factor weights

% Allocation of monitoring variables
fetchthese=true(1,nmodels); % fetchNext: only these (not cancelled)
completed    = false(nmodels,1);       % Complete / incomplete?
numCompleted = 0;                      % Total number of completed models
ttc=repmat(duration,nmodels,1);        % time-to-convergence for each run

% Allocation of variables for prelim. plotfacs
[facs,iF]=unique(factorarray);
idx=nan(numel(facs),1);
idxOld=nan(numel(facs),1);

% 1st Blockbar
state=false(numel(facs),nmodels/numel(facs));
mname=cellstr(strcat(repmat('Model',numel(facs),1),num2str((facs))))';
% Close any old blockbar
figHandles = get(0,'Children');
for n=1:numel(figHandles)
    if strcmp(figHandles(n).Name,'Progress...')
        close(figHandles(n));
    end
end
clearvars n figHandles
blockbar(mname,state);


while numCompleted < nmodels
    ffetch=output(fetchthese&~completed');
    [completedIdx,Zhat,~,stats] = fetchNext(ffetch,0.1);
    % If fetchNext returned an output, let's extract it.
    if ~isempty(completedIdx)
        % Get actual index of future if ffetch-modout were subset
        [~,completedIdx]=intersect([output.ID],ffetch(completedIdx).ID);
        numCompleted = numCompleted + 1;
        % Update list of completed modout.
        completed(completedIdx) = true;
        % Update state to account for completed modout
        state=reshape(completed,nmodels/numel(facs),numel(facs))';
        
        Model{completedIdx,1} = rcvec([Zhat{1}.U(1:3);Zhat{2}.U(2)],'row');
        Iter{completedIdx,1} = stats.info_lbfgsb.iterations;
        Err{completedIdx,1} = stats.F;
        fweights{completedIdx,1}=[Zhat{1}.lambda Zhat{2}.lambda];
        ttc(completedIdx)=datetime(output(completedIdx).FinishDateTime,'TimeZone','Europe/London')-...
            datetime(output(completedIdx).StartDateTime,'TimeZone','Europe/London');
        
         
        disp(['mod-id ',sprintf('%03d',completedIdx),' done. | #it.: ',...
            sprintf('%-*s',4,num2str(Iter{completedIdx,1})),' | err.: ',num2str(Err{completedIdx,1}(1)),...
            ' | time: ',char(ttc(completedIdx)),...
            ' | sec./it.: ',num2str(round(seconds(ttc(completedIdx))/Iter{completedIdx,1},2)),...
             ]);
        
     end
    % Check status of blockbar
    c=blockbar(mname,state);
    
    % Check if any models were cancelled and cancel the unfinished jobs among these models
    fetchthese=~repelem(c,nmodels/numel(unique(factorarray)),1)';
    if any(~fetchthese)
        if ~isempty(output(~fetchthese&~completed'))
            cancel(output(~fetchthese&~completed'));
            Err(~fetchthese&~completed',1)=num2cell(nan(sum(~fetchthese&~completed'),1));
            numCompleted=numCompleted+numel(output(~fetchthese&~completed'));
            completed(~fetchthese) = true;
            warning('User canceled some models prematurely. No output fetched for those.')
        end
    end
    
    
    for n=1:numel(facs)
        try
            if ~all(isnan(cell2mat(Err(factorarray==facs(n)))))&all(completed(factorarray==facs(n))) %#ok<AND2>
                temp=cell2mat(Err(factorarray==facs(n)));
                [~,idx(n)]=min(temp(:,1));
                idx(n)=idx(n)+iF(n)-1;
            else
                idx(n)=nan;
            end
        catch
            disp('asd')
        end
    end
    if ~isequal(isnan(idxOld),isnan(idx))&&~all(isnan(idx))
        try
            preliminaryplots(Model(idx(~isnan(idx))),factorarray(idx(~isnan(idx)))',fweights(idx(~isnan(idx))),data)
        catch
            warning('Plotting error (preliminary model outcome)')
        end
        idxOld=idx;
    end
    
end
% If complete, cancel the modout and delete the waitbar.
cancel(output);
blockbar('close');
end

function [cncl] = blockbar(names,state)
% Blockbar is similar to a multiwaitbar, but visualizes the state of
% individual jobs rather than a % completed bar.
% (c) Urban W�nsch, 2019
%% Default options
incompcol = [0.8         0.8         0.8];
compcol   = [0.204       0.569           1];

multi=numel(names);
init=true; % init state is true by default. When old figure is found, init = false.
cncl=false(numel(names),1);
%% Find existing blockbar and axes within it (not the buttons)
figHandles =  get(groot, 'Children');
for n=1:numel(figHandles)
    if strcmp(figHandles(n).Name,'Progress...')
        init=false; % Success, do not make another figure, use the old
        old=figHandles(n);
        break
    end
end
% Close blockbar if 'close' is requested by user
if strcmp(class(names),'char')&&sum(strcmp(names,'close'))==1
    try
       close(old)
       return
    catch
        warning('Coudn''t close the blockbar. Probably, there was none.')
        return
    end
end
% Check input
if numel(names)~=size(state,1)
    warning('Size of ''names'' and ''state'' are incompatible. Can'' draw the blockbar...')
    return
end
%% Draw the blockbar
stp=[0:1/size(state,2):1 1];
if init % This generates the figure and bars
    set(0, 'Units', 'pixel');
    screenSize = get(0,'ScreenSize');
    pointsPerPixel = 72/get(0,'ScreenPixelsPerInch');
    width = 360 * pointsPerPixel;
    height = multi * 75 * pointsPerPixel;
    fpos = [screenSize(3)/2-width/2 screenSize(4)/2-height/2 width height];
    figureh=figure('Position',fpos,...
        'MenuBar','none',...
        'Numbertitle','off',...
        'Name','Progress...',...
        'Resize','off');
    axeswidth = 172;
    axesheight = 172/14;
    axesbottom = fliplr(linspace(15,fpos(4)-axesheight*3,multi));
    addprop(figureh,'state');
    addprop(figureh,'sname');
    figureh.state=state;
    figureh.sname=names;
    for i=1:numel(names)
        axPos = [axesheight*2.5 axesbottom(i) axeswidth axesheight];
        bPos = [axPos(1)+axPos(3)+10 axPos(2) 50 axPos(4)*1.5];
        bPos(2) = bPos(2)-0.5*(bPos(4)-axPos(4));
        ax(i)=axes('units','pixel','pos',axPos);
        addprop(ax(i),'sname');
        set(ax(i),'units','pixel','Tag',names{i});
        uic=uicontrol( 'Style', 'togglebutton', ...
            'String', '', ...
            'FontWeight', 'Bold', ...
            'units','pixel',...
            'Position',bPos,...
            'Tag',num2str(i),...
            'String','Cancel','FontWeight','normal');
        addprop(uic,'snames');
        uic.snames=i;
        
        title(ax(i),names{i});
        set(ax(i),'YTickLabel','');
        set(ax(i),'XTickLabel','');
        for n=1:size(state,2)
            if state(i,n)
                col=compcol;
            else
                col=incompcol;
            end
            p=patch(ax(i),[stp(n) stp(n+1) stp(n+1) stp(n)],[0 0 1 1],col,...
                'EdgeColor','none');hold on
            addprop(p,'id');
            p.id=n;
        end
        set(ax(i),'YColor',[0 0 0 0.5],'XColor',[0 0 0 0.5],'Box','on','YTick','','XTick','')
    end
else % This updates  the figure and bars
    oldstate=old.state;
    if ~isequal(oldstate,state)
        ax=get(old,'Children');
        ax=ax(strcmp(get(ax,'Type'),'axes'));
        ni=multi:-1:1;
        for n=1:numel(names)
            newstate=state(n,:);
            if ~isequal(oldstate,newstate)
                h=get(ax(ni(n)),'Children');
                ii=size(state,2):-1:1;
                for i=1:size(state,2)
                    if state(n,i)&&~oldstate(n,i)
                        h(ii(i)).FaceColor=compcol;
                    end
                end
            end
        end
        old.state=state;
    end
    childs=get(old,'Children');
    tbutt=childs(strcmp(get(childs,'Type'),'uicontrol'));
    ni=multi:-1:1;    
    for n=1:numel(names)
        if logical(tbutt(ni(n)).Value)&any(~state(n,:))
            cncl(n)=true;
        else
            cncl(n)=false;
        end
    end
    
end
end

function preliminaryplots(m,f,wt,data)
% Plot models for which all starts have converged before all other models
% have finished.

% Identify previous figure windows and close them
figHandles = get(0,'Children');
if any(contains({figHandles.Name},'Preliminary'))
    if sum(contains({figHandles.Name},'Preliminary'))>1
        close(figHandles(contains({figHandles.Name},'Preliminary')))
        hf=figure;
        h=.8;w=.4;b=(1-w)/2;l=(1-h)/2;
        set(hf, 'units','normalized','outerposition',[l b w h],...
            'Name','Preliminary ACMTF modeling outcome');
        movegui('center')
    else
        ax = (findobj(figHandles(contains({figHandles.Name},'Preliminary')), 'type', 'axes'));
        for n=1:numel(ax)
            delete(ax(n))
        end
        figure(figHandles(contains({figHandles.Name},'Preliminary')));
    end
else
    hf=figure;
    h=.8;w=.4;b=(1-w)/2;l=(1-h)/2;
    set(hf, 'units','normalized','outerposition',[l b w h],...
        'Name','Preliminary ACMTF modeling outcome');
    movegui('center')
end



nfac=length(f);
t=tiledlayout(5,nfac);
t.Padding = "compact";
t.TileSpacing = "compact";
tilelayout=vec(reshape(1:nfac*5,nfac,5)');
cnt=1;
for j=1:nfac
    [A,B,C,D]=fac2let(m{j});
    nexttile(tilelayout(cnt))
    cnt=cnt+1;
    
    hp{1}=plot(A,'LineWidth',1.2,'Marker','.','MarkerSize',7);title('Scores');
    nexttile(tilelayout(cnt))
    cnt=cnt+1;

    
    
    hp{2}=plot(B,'-','LineWidth',1.2);title('Load (tensor mode 2)');
    nexttile(tilelayout(cnt))
    cnt=cnt+1;

    
    
    hp{3}=plot(C,'-','LineWidth',1.2);title('Load (tensor mode 3)');
    nexttile(tilelayout(cnt))
    cnt=cnt+1;

    
    
    hp{4}=plot(D,'-','LineWidth',1.2);title('Load (matrix mode 2)');
    nexttile(tilelayout(cnt))
    cnt=cnt+1;
    
    hold on
    col=lines(f(j));
    for k=1:f(j)
        bh{k}=bar(k,wt{j}(k,:));
        bh{k}(1).FaceColor=col(k,:);
        bh{k}(2).FaceColor=col(k,:);
        xpt(k,:)=[bh{k}(:).XEndPoints];
    end
    lab=[cellstr(strcat(repmat('T_{C',f(j),1),num2str([1:f(j)]'),repmat('}',f(j),1)))...
        cellstr(strcat(repmat('M_{C',f(j),1),num2str([1:f(j)]'),repmat('}',f(j),1)))];
    lab=lab(:);[~,i]=sort(xpt(:));lab=lab(i);
    set(gca,'XTick',sort(xpt(:)),...
        'XTickLabel',lab)
    
    for n=1:4
        set(hp{n}, {'color'}, num2cell(lines(f(j)),2));
    end
end
if any(contains({figHandles.Name},'Progress...'))
    figure(figHandles(contains({figHandles.Name},'Progress...')))
end

end

function [dataout]=analyzeresults(data,f,factorarray,starts,maxit,convgcrit,constraints,outcome)

dataout=data;


for j=1:numel(f)
    i=find(factorarray==f(j)); % Find f(j)-component models
    errsub=outcome{3}(i);
    errsub=cell2mat(errsub);
    errsub=errsub(:,1);
    [~,mini]=min(errsub);
    mini=i(mini);
    
    if all(isnan(errsub)) % No models produced. Skip this one.
        out.loads    = [];
        out.itr      = [];
        out.wts      = [];
        out.err      = [];
        out.percexpl = [];
        out.all      = [];
        dataout.acmtf(f(j))=out;
        continue
    end
    out.loads    = outcome{1}{mini};
    out.itr      = outcome{2}{mini};
    out.wts      = outcome{4}{mini};
    
    
    [xm,ym]=xymodel(out.loads,out.wts');
    ssq = @(x) sum(x(:).^2,'omitnan'); % Anonymous function for sum of squared values
    
    out.err      = [ssq(data.t.data(~isnan(data.t.data))/data.z.norms(1)-xm(~isnan(data.t.data))) ssq(data.m.data(~isnan(data.m.data))/data.z.norms(2)-ym(~isnan(data.m.data)))];
    out.percexpl = [100 * (1 - (out.err(1) / ssq(data.t.data/data.z.norms(1)))) ... 
                   100 * (1 - (out.err(2) / ssq(data.m.data/data.z.norms(2))))];
    out.all.loads=outcome{1}(i);
    out.all.itr=outcome{2}(i);
    out.all.err=outcome{3}(i);
    out.all.wts=outcome{4}(i);
    
    dataout.acmtf(f(j))=out;
    clearvars out
end
end