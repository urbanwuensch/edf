function [ Z ] = xy2z( x,y )


Z.object{1} = x;
Z.object{2} = y;

% missing entries
for i=1:length(Z.object)
    if ~isempty(find(isnan(Z.object{i})==1))
        Zmiss       = Z.object{i};    
        W           = ones(size(Zmiss));
        Worg        = find(isnan(Zmiss)==1); 
        Zmiss(Worg) = 0;
        W(Worg)     = 0;
        W           = tensor(W);
        Z.miss{i}   = W;    
        Z.object{i} = tensor(Zmiss);
    else
        Z.object{i} = tensor(Z.object{i});
        Z.miss{i}   =[];
    end    
end

Z.modes ={[1 2 3],[1 4]};
Z.size = [size(Z.object{1}.data) size(Z.object{2}.data,2)]; 

%normalize data sets (equal signal magintude for both x and y)
for i=1:2
    norms(i)    = norm(Z.object{i});
    Z.object{i} = Z.object{i}/norms(i);
end
Z.norms=norms;
end