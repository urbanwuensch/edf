function [containerout] = add2container(container,varargin)
%% Add data to existing contaier or replace existing data
%
%
%
%% Input parsing

params = inputParser;
addParameter(params,'data', @checkdata);
addParameter(params,'title', @ischar);
addParameter(params,'label', @checklabel);
addParameter(params,'axis', @checkaxis);
addParameter(params,'type', @checktype);
parse(params,varargin{:});

data=params.Results.data;
title=params.Results.title;
label=params.Results.label;
axis=params.Results.axis;
type=params.Results.type;

szdata=size(data);


%% Check inputs beyond initial validation
if numel(label)~=numel(szdata)
    error(' The number of entries in ''label'' does not match the numer of dimensions of ''data''')
end
if numel(axis)~=numel(szdata)
    error(' The number of entries in ''axis'' does not match the numer of dimensions of ''data''')
else
    for j=1:numel(szdata)
        if ~szdata(j)==numel(axis{j})
            error(['Size of ''data'' and axis does not match in the ',num2str(j),'th dimension'])
        end
    end
end
if numel(type)~=numel(szdata)
    error(' The number of entries in ''type'' does not match the numer of dimensions of ''data''')
end

%% Assign
containerout=container;
if numel(szdata)==2
    field='m';
elseif numel(szdata)==3
    field='t';
else
    error(['''data'' must have 2 or 3 modes. ''data'' currently has ',num2str(numel(szdata)),'modes',])
end

if ~isempty(containerout.(field).data)
    warning('There was already some data in the strucutre. Multiple data sources of the same type are currently not supported. Data was overwritten.')
end

containerout.(field).data=data;
containerout.(field).title=title;
containerout.(field).label=label;
containerout.(field).axis=axis;
containerout.(field).type=cellstr(repmat('continuous',numel(szdata),1))';

if isnan(containerout.nSample)
    containerout.nSample=size(containerout.(field).data,1);
elseif isnumeric(containerout.nSample)
    if ~containerout.nSample==size(containerout.(field).data,1)
        error(['nSample already set to ',num2str(containerout.nSample),'. ''data'' has a size of ',num2str(size(containerout.(field).data,1)),' in the first mode.'])
    end
end

containerout.lastmodified=string(datetime);
containerout.history{end+1,1}=strcat(string(datetime),": Added / replaced data in the field ",field);
end

function result=checkdata(in)

result=true;

if ~isnumeric(in)
    result=false; %#ok<NASGU>
    error('Input to ''data'' must be numeric.')
end

if ndims(in)>3
   result=false; %#ok<NASGU>
   error('Input to ''data'' must have less than 3 modes.')
end

if ismatrix(in)&&any(size(in)==1)
    result=false; %#ok<NASGU>
    error('Input to ''data'' must have at least two non-singelton dimensions.')
end

end

function result=checklabel(in)

result=true;

if ~iscell(in)
    result=false; %#ok<NASGU>
    error('Input to ''label'' must be a cell array.')
end

for j=1:numel(in)
    if ~ischar(in{j})
        result=false; %#ok<NASGU>
        error('All input to ''label'' in each cell must be characters (satisfy ''ischar'').')
    end
end
end

function result=checkaxis(in)

result=true;

if ~iscell(in)
    result=false; %#ok<NASGU>
    error('Input to ''axis'' must be a cell array.')
end

for j=1:numel(in)
    if ~isnumeric(in{j})
        result=false; %#ok<NASGU>
        error('All input to ''axis'' in each cell must be numeric (satisfy ''ischar'').')
    end
end
end

function result=checktype(in)

result=true;

if ~iscell(in)
    result=false; %#ok<NASGU>
    error('Input to ''type'' must be a cell array.')
end

for j=1:numel(in)
    if ~any(strcmp(s,{'discrete','continuous'}))
        result=false; %#ok<NASGU>
        error('All input to ''type'' in each cell must be ''discrete'' or ''continuous.''')
    end
end
end
