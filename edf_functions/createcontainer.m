function dsOut = createcontainer

% Create structure
dsOut=struct;
% Set basic information
dsOut.datecreated=string(datetime);
dsOut.lastmodified=string(datetime);
dsOut.author=[char(java.lang.System.getProperty('user.name')) char(java.net.InetAddress.getLocalHost.getHostName)];
dsOut.history{1}=strcat(string(datetime),": (Empty) dataset created.");

% Create data & relevant variables
t=struct;
m=struct;
fields={'data','title','label','axis','type'};
for j=1:numel(fields)
    t.(fields{j})=[];
    m.(fields{j})=[];
end
dsOut.t=t;
dsOut.m=m;
dsOut.nSample=NaN;
dsOut.samplemetadata=table;
end

