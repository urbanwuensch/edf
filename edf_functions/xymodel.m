function [ modeledX,modeledY,fractionY] = xymodel(model,weights)
% acmtf equivalent of nmodel
[A,B,C,D]=fac2let(model);

modeledX=zeros(size(A,1),size(B,1),size(C,1));
for n=1:size(A,1)
    for i=1:size(A,2)
       modeledX(n,:,:)=squeeze(modeledX(n,:,:))+...
                        weights(1,i)*A(n,i)*B(:,i)*C(:,i)';
    end
end

modeledY=zeros(size(A,1),size(D,1));
fractionY=zeros(size(A,2),size(A,1),size(D,1));

for n=1:size(A,1)
    for i=1:size(A,2)
        modeledY(n,:)=squeeze(modeledY(n,:))+...
                        weights(2,i)*A(n,i)*D(:,i)';
        fractionY(i,n,:)=weights(2,i)*A(n,i)*D(:,i)';
    end
end

end

