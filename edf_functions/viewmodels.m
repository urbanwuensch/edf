function viewmodels(models)
%% Find which models have been calculated
f=find(arrayfun(@(n) ~isempty(models.acmtf(n).itr),1:size(models.acmtf,2)));
ncomp=numel(f);

%% Set up the figure
fig = figure;
set(fig,'InvertHardcopy','off','Color',[1 1 1]);
w=.4;h=.7;l=(1-w)/2;b=(1-h)/2;
set(fig, 'units','normalized','outerposition', [l b w h]);

%% Set up the tabs and elements within
tabgp = uitabgroup(fig,'Position',[.00 .00 1 1]);
tab(1) = uitab(tabgp,'Title','Overview: Loadings');
tab(2) = uitab(tabgp,'Title','Overview: Residuals');
tab(3) = uitab(tabgp,'Title','Focus: Loadings');
tab(4) = uitab(tabgp,'Title','Focus: Residuals');
tab(5) = uitab(tabgp,'Title','Focus: Uniqueness');

% tab(1): Overview of loadings
t(1) = tiledlayout(tab(1),5,ncomp,'padding','compact','TileSpacing','compact');
for n=1:5*ncomp,ax{1}(n)=nexttile(t(1));end

% tab(2): Overview of residuals
t(2) = tiledlayout(tab(2),5,ncomp,'padding','compact','TileSpacing','compact');
for n=1:5*ncomp,ax{2}(n)=nexttile(t(2));end

% tab(3): Focus on individual model loadings
pan{3}(1) = uipanel(tab(3),'Position',[.2 .05 .6 .8]);
t(3) = tiledlayout(pan{3}(1),5,1,'padding','compact','TileSpacing','compact');
for n=1:5,ax{3}(n)=nexttile(t(3));end
pan{3}(2) = uipanel(tab(3),'Position',[.4 .9 .2 .06]);
uicontrol(pan{3}(2),'Style','popupmenu','String',...
    cellstr(strcat(num2str(f'),repmat('-Comp.',numel(f),1))),...
    'Units','normalized',...
    'position',[0.0 0.0 0.9 0.9],...
    'Callback',{@focusload,ax{3},models,f});

% tab(4): Focus on individual model residuals
pan{4}(1) = uipanel(tab(4),'Position',[.2 .05 .6 .8]);
t(4) = tiledlayout(pan{4}(1),5,1,'padding','compact','TileSpacing','compact');
for n=1:5,ax{4}(n)=nexttile(t(4));end
pan{4}(2) = uipanel(tab(4),'Position',[.4 .9 .2 .06]);
uicontrol(pan{4}(2),'Style','popupmenu','String',...
    cellstr(strcat(num2str(f'),repmat('-Comp.',numel(f),1))),...
    'Units','normalized',...
    'position',[0.0 0.0 0.9 0.9],...
    'Callback',{@focusresi,ax{4},models,f});

% tab(5): Focus on model uniqueness
pan{5}(1) = uipanel(tab(5),'Position',[.1 .05 .8 .75]);
t(5) = tiledlayout(pan{5}(1),5,2,'padding','none','TileSpacing','compact');
ax{5}(7)=nexttile(t(5),[1 2]);
ax{5}(1)=nexttile(t(5),[1 1]);
ax{5}(5)=nexttile(t(5),[2 1]);
ax{5}(2)=nexttile(t(5),[1 1]);
ax{5}(3)=nexttile(t(5),[1 1]);
ax{5}(6)=nexttile(t(5),[2 1]);
ax{5}(4)=nexttile(t(5),[1 1]);

pan{5}(2) = uipanel(tab(5),'Position',[.2 .9 .6 .06]);

uicontrol(pan{5}(2),'Style','popupmenu','String',...
    cellstr(strcat(num2str(f'),repmat('-Comp.',numel(f),1))),...
    'Units','normalized',...
    'position',[0.0 0.0 0.2 0.9],...
    'Tag','componentmenu',...
    'Callback',@popupcb)

uicontrol(pan{5}(2),'Style','text',...
    'String','err-min(err) threshold:',...
    'Units','normalized',...
    'BackgroundColor',[1 1 1],...
    'position',[0.25 0.0 0.2 0.9])

c = uicontrol(pan{5}(2),'Style','edit',...
    'Units','normalized',...
    'Tag','cutoffbox',...
    'position',[0.45 0.1 0.2 0.8],...
    'String','1e-6',...
    'Callback',@textboxcb);
    
uicontrol(pan{5}(2),'Style','pushbutton',...
    'Units','normalized',...
    'position',[0.8 0.1 0.1 0.8],...
    'String','Go!',...
    'Callback',{@focusuniqueness,ax{5},models,f});

uicontrol(c);

for j=1:5
    for k=1:2
        pan{j}(k).BackgroundColor=[1 1 1];
        pan{j}(k).BorderType='none';
    end
end

for j=1:5
    tab(j).BackgroundColor=[1 1 1];
end
%% Plot the overview graphs
acnt1=1;
acnt2=1+ncomp;
acnt3=acnt2+ncomp;
acnt4=acnt3+ncomp;
acnt5=acnt4+ncomp;
lineandmarker=discreteorcontinuous([models.t.type models.m.type]);
for n=f
    plot(ax{1}(acnt1),models.acmtf(n).loads{1},'LineStyle',lineandmarker{1}{1},'Marker',lineandmarker{1}{2})
    xlabel(ax{1}(acnt1),models.t.label{1})
    acnt1=acnt1+1;
    plot(ax{1}(acnt2),models.acmtf(n).loads{2},'LineStyle',lineandmarker{2}{1},'Marker',lineandmarker{2}{2})
    xlabel(ax{1}(acnt2),models.t.label{2})
    acnt2=acnt2+1;
    plot(ax{1}(acnt3),models.acmtf(n).loads{3},'LineStyle',lineandmarker{3}{1},'Marker',lineandmarker{3}{2})
    xlabel(ax{1}(acnt3),models.t.label{3})
    acnt3=acnt3+1;
    plot(ax{1}(acnt4),models.acmtf(n).loads{4},'LineStyle',lineandmarker{4}{1},'Marker',lineandmarker{4}{2})
    xlabel(ax{1}(acnt4),models.m.label{2})
    acnt4=acnt4+1;
    
    hold(ax{1}(acnt5),'on')
    col=lines(n);
    for k=1:n
        bh{k}=bar(ax{1}(acnt5),k,models.acmtf(n).wts(k,:));
        bh{k}(1).FaceColor=col(k,:);
        bh{k}(2).FaceColor=col(k,:);
        xpt(k,:)=[bh{k}(:).XEndPoints];
    end
    lab=[cellstr(strcat(repmat('T_{C',n,1),num2str([1:n]'),repmat('}',n,1)))...
        cellstr(strcat(repmat('M_{C',n,1),num2str([1:n]'),repmat('}',n,1)))];
    lab=lab(:);[~,i]=sort(xpt(:));lab=lab(i);
    set(ax{1}(acnt5),'XTick',sort(xpt(:)),...
        'XTickLabel',lab)
    
    
    xlabel(ax{1}(acnt5),'Component')
    ylabel(ax{1}(acnt5),'Component weights')
    acnt5=acnt5+1;
end
for j=1:ncomp:5*ncomp
    ylabel(ax{1}(j),'Loadings')
end


acnt1=1;
acnt2=1+ncomp;
acnt3=acnt2+ncomp;
acnt4=acnt3+ncomp;
acnt5=acnt4+ncomp;
for n=f
    [xm,ym]=xymodel(models.acmtf(n).loads,models.acmtf(n).wts');
    
    xr=models.t.data./models.z.norms(1)-xm;
    yr=models.m.data./models.z.norms(2)-ym;
    ssrx{1} = squeeze(nansum(nansum(xr.^2,1),2));
    ssrx{2} = squeeze(nansum(nansum(xr.^2,1),3));
    ssrx{3} = squeeze(nansum(nansum(xr.^2,2),3));
    ssry{1} = squeeze(nansum(yr.^2,1));
    ssry{2} = squeeze(nansum(yr.^2,2));

    
    plot(ax{2}(acnt1),ssrx{3},'LineStyle',lineandmarker{1}{1},'Marker',lineandmarker{1}{2},'Color',[.2 .2 .2])
    xlabel(ax{2}(acnt1),models.t.label{1})
    acnt1=acnt1+1;
    plot(ax{2}(acnt2),ssrx{2},'LineStyle',lineandmarker{2}{1},'Marker',lineandmarker{2}{2},'Color',[.2 .2 .2])
    xlabel(ax{2}(acnt2),models.t.label{2})
    acnt2=acnt2+1;
    plot(ax{2}(acnt3),ssrx{1},'LineStyle',lineandmarker{3}{1},'Marker',lineandmarker{3}{2},'Color',[.2 .2 .2])
    xlabel(ax{2}(acnt3),models.t.label{3})
    acnt3=acnt3+1;
    plot(ax{2}(acnt4),ssry{2},'LineStyle',lineandmarker{1}{1},'Marker',lineandmarker{1}{2},'Color',[.2 .2 .2])
    xlabel(ax{2}(acnt4),models.t.label{1})
    acnt4=acnt4+1;
    plot(ax{2}(acnt5),ssry{1},'LineStyle',lineandmarker{4}{1},'Marker',lineandmarker{4}{2},'Color',[.2 .2 .2])
    xlabel(ax{2}(acnt4),models.m.label{2})
    acnt5=acnt5+1;
    
    clearvars ssrx ssry xr yr xm ym
end


end

function focusload(src,~,ax,models,f)

% Retreive the selected model
try
    val = src.Value;
catch
    val=1;
end
f=f(val);
lineandmarker=discreteorcontinuous([models.t.type models.m.type(2)]);
axvals=[models.t.axis models.m.axis(2)];
axlabs=[models.t.label models.m.label(2)];

% model loadings
for j=1:4
    plot(ax(j),axvals{j},models.acmtf(f).loads{j},lineandmarker{j}{1},'Marker',lineandmarker{j}{2})
    xlabel(ax(j),axlabs{j})
end
% Model weights
hold(ax(5),'on')
col=lines(f);
for k=1:f
    bh{k}=bar(ax(5),k,models.acmtf(f).wts(k,:));
    bh{k}(1).FaceColor=col(k,:);
    bh{k}(2).FaceColor=col(k,:);
    xpt(k,:)=[bh{k}(:).XEndPoints];
end
lab=[cellstr(strcat(repmat('T_{C',f,1),num2str([1:f]'),repmat('}',f,1)))...
    cellstr(strcat(repmat('M_{C',f,1),num2str([1:f]'),repmat('}',f,1)))];
lab=lab(:);[~,i]=sort(xpt(:));lab=lab(i);
set(ax(5),'XTick',sort(xpt(:)),...
    'XTickLabel',lab)

end

function focusresi(src,~,ax,models,f)

% Retreive the selected model
try
    val = src.Value;
catch
    val=1;
end

f=f(val);
lineandmarker=discreteorcontinuous([models.t.type models.m.type]);
axvals=[models.t.axis models.m.axis];
axlabs=[models.t.label models.m.label];

[xm,ym]=xymodel(models.acmtf(f).loads,models.acmtf(f).wts');
xr=models.t.data./models.z.norms(1)-xm;
yr=models.m.data./models.z.norms(2)-ym;
ssr{1} = squeeze(nansum(nansum(xr.^2,2),3));
ssr{2} = squeeze(nansum(nansum(xr.^2,1),3));
ssr{3} = squeeze(nansum(nansum(xr.^2,1),2));
ssr{4} = squeeze(nansum(yr.^2,2));
ssr{5} = squeeze(nansum(yr.^2,1));

% model residuals
for j=1:5
    if j<4
        plot(ax(j),axvals{j},ssr{j},'LineStyle',lineandmarker{j}{1},'Marker',lineandmarker{j}{2},'Color',[.2 .2 .2])
        xlabel(ax(j),axlabs{j})
    elseif j==4
        plot(ax(j),axvals{1},ssr{j},'LineStyle',lineandmarker{1}{1},'Marker',lineandmarker{1}{2},'Color',[.2 .2 .2])
        xlabel(ax(j),axlabs{1})
    elseif j==5
        plot(ax(j),axvals{5},ssr{j},'Color',[.2 .2 .2])
        xlabel(ax(j),axlabs{5})
    end
end
end


function focusuniqueness(~,~,ax,models,f)

% Retreive the selected model

h = findobj('Tag','componentmenu');
val = h.UserData;

if isempty(val)
    val=h.Value;
end

h = findobj('Tag','cutoffbox');
cutoff = h.UserData;
if isempty(cutoff)
    cutoff = str2double(h.String);
end

% Define variables / functions
f=f(val);
nmodel=numel(models.acmtf(f).all.itr);

if nmodel<10
    warning('Increase the number of models to at least 10. Currently we don''t have enough models to investigate uniqueness.')
    return
end
similarity=@(f1,f2) f1'*f2/(sqrt(f1'*f1)*sqrt(f2'*f2));

% Extract data (for easier access)
for j=1:nmodel
    wts{j}=models.acmtf(f).all.wts{j};
    loads{j}=models.acmtf(f).all.loads{j};
    err(j,1)=models.acmtf(f).all.err{j};
end

take=err-min(err)<cutoff;
plterr=err-min(err);
i=1:nmodel;
cla(ax(7))
hold(ax(7),'on')
plot(ax(7),1:nmodel,plterr,'Color',[.2 .2 .2],'LineWidth',1.5,'Marker','+','LineStyle','none')
plot(ax(7),i(take),plterr(take),'Color',[1 0.4 0.4],'LineWidth',1.5,'Marker','o','LineStyle','none')
yline(ax(7),cutoff,'LineStyle','--','Color',[1 0.4 0.4]);
ylim(ax(7),[min(plterr)-0.1*min(plterr) max(plterr)+0.1*max(plterr)])
title(ax(7),'Distance of all from the best solution (red = part of comparison).')
ylabel(ax(7),'Distance')
xlabel(ax(7),'Solution #')
set(ax(7),'YScale','log')
axlabs=[models.t.label models.m.label];


wts(~take)=[];
loads(~take)=[];
err(~take)=[];
nmodel=nmodel-sum(~take);
clearvars simi
% Calculate factor similarity
for j=1:f % Counter for ncomp (reference)
    for k=1:f % Counter for ncomp (target)
        for o=1:nmodel % Counter for nmodel (all)
            for l=1:4 % Mode counter
                simi(o,l,j,k)=similarity(models.acmtf(f).loads{l}(:,j),loads{o}{l}(:,k));
            end
        end
    end
end

for o=1:nmodel % Counter for nmodel (all)
    for j=1:f % Counter for ncomp (reference)
        [~,i]=max(mean(squeeze(simi(o,:,:,:)),1),[],3);
        reord(o,:)=i;       
    end
end
clearvars simi
for o=1:nmodel
    for k=1:f
        for l=1:4
            simi(o,k,l)=similarity(models.acmtf(f).loads{l}(:,k),loads{o}{l}(:,reord(o,k)));
        end
    end
end
for k=1:4
    h=errorbar(ax(k),squeeze(mean(simi(:,:,k))),squeeze(std(simi(:,:,k))),...
        'LineStyle','none','Color',[.2 .2 .2],'LineWidth',1.5,'Marker','d','MarkerEdgeColor',[.2 .2 .2]);
    xlim(ax(k),[0.5 f+0.5])
    if min(h.YData)<0.5
        ylim(ax(k),[min(h.YData)-0.1*min(h.YData) 1])
    else
        ylim(ax(k),[0.95 1])
    end
    ylabel(ax(k),'Similarity')
    title(ax(k),axlabs{k})
end
xlabel(ax(4),'Component')
for j=1:nmodel
    wtsx(j,:)=wts{j}(reord(j,:),1);
    wtsy(j,:)=wts{j}(reord(j,:),2);
end
cla(ax(5))
hold(ax(5),'on')
bar(ax(5),mean(wtsx),'FaceColor',[0.2 0.6 1]);
errorbar(ax(5),1:f,mean(wtsx),std(wtsx),'LineStyle','none','Color',[.2 .2 .2],'LineWidth',1.5);
xlim(ax(5),[0.5 f+0.5])
xlabel(ax(5),'Component')
ylabel(ax(5),'Weight')
title(ax(5),'Tensor factor weights')
hold(ax(5),'off')

cla(ax(6))
hold(ax(6),'on')
bar(ax(6),mean(wtsy),'FaceColor',[0.992 0.42 0.42]);
errorbar(ax(6),1:f,mean(wtsy),std(wtsy),'LineStyle','none','Color',[.2 .2 .2],'LineWidth',1.5);
xlim(ax(6),[0.5 f+0.5])
xlabel(ax(6),'Component')
ylabel(ax(6),'Weight')
title(ax(6),'Matrix factor weights')
hold(ax(6),'off')

for j=1:numel(ax)
    set(ax(j),'XTick',1:f)
end

end
function lineandmarker=discreteorcontinuous(types)
lineandmarker=cell(numel(types),1);
for j=1:numel(types)
    switch types{j}
        case 'continuous'
            lineandmarker{j}={'-','none'};
        case 'discrete'
            lineandmarker{j}={'-','+'};
        otherwise
            error('''axestypes'' property must be ''continuous'' or ''discrete''')
    end
end
end

function popupcb(hObject,~)
	sval = hObject.Value;
	hObject.UserData = sval;
end

function textboxcb(hObject,~)
	sval = hObject.String;
    sval = str2double(sval);
    if ~isempty(sval)
        hObject.UserData = sval;
    else
        hObject.String='1e-3';
        sval = hObject.String;
        sval = str2double(sval);
        hObject.UserData = sval;
    end
end