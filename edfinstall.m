function edfinstall
% Installation and initialization routine

%% Initial check before anything else
% Check if the highest priority dreeminstall is in the current folder & if
% this specific function being executed is also the one in the current
% path.
if all([~strcmp(fileparts(which('edfsinstall.m')),pwd),~strcmp(fileparts(mfilename('fullpath')),pwd)])
    error('You must change the MATLAB directory to the one containing ''edfsinstall'' in order to correctly install the toolbox')
end

if ~checkonlinestatus
    warning('Cannot reach ''google.com''. Your computer should have internet access for this function to work.')
end

%% STEP 1
disp('Step 1/x: Checking your MATLAB version');pause(0.2)

mver=ver('MATLAB');
if str2double(mver.Version)<9.7
    warning(['Consider updating MATLAB in order for all functions to work as intended. This toolbox requires v', vers ,' R2019b or higher. Current version: ',mver.Release(2:end-1)])
end
disp(['     ','Success.'])
% mallver=ver;
% tbs={'Statistics and Machine Learning Toolbox' 'Parallel Computing Toolbox' 'Curve Fitting Toolbox'};
% isthere=zeros(1,numel(tbs));
% for n=1:numel(tbs)
%     isthere(n)=any(~cellfun(@isempty,strfind({mallver.Name},tbs{n})));
% end
% if all(isthere)
%     disp('          Success.')
% else
%     warning(['Missing toolbox(es):',tbs{~isthere},'. Some functions may not work as intended.'])
%     disp('Press any key to acknowledge.')
%     pause
% end

%% STEP 2
disp('Step 2/x: Checking your MATLAB version and installed MATLAB products');pause(0.2)

tbxreq=table;
tbxreq.func={'cmtf_fun';'poblano_params';'lbfgsb';'cp_als'};
tbxreq.name={'The MATLAB CMTF Toolbox';'Poblano Toolbox for Matlab';'L-BFGS-B';'Tensor toolbox'};
tbxreq.url={'http://models.life.ku.dk/joda/CMTF_Toolbox';'http://software.sandia.gov/trac/poblano';'https://github.com/stephenbeckr/L-BFGS-B-C';'https://www.tensortoolbox.org/index.html'};
tbxreq.dlurl={'https://dl.dropboxusercontent.com/s/1zkm0jxcl8962xk/CMTF_to_be_released.zip';'https://github.com/sandialabs/poblano_toolbox/archive/v1.2.zip';'https://github.com/stephenbeckr/L-BFGS-B-C/archive/master.zip';'https://gitlab.com/tensors/tensor_toolbox/-/archive/v3.1/tensor_toolbox-v3.1.zip'};
tbxreq.foldername={'CMTF_to_be_released';'poblano_toolbox-1.2';'L-BFGS-B-C-master';'tensor_toolbox-v3.1'};
tbxreq.installed=[false;false;false;false];
for j=1:height(tbxreq)
    if ~exist(tbxreq.func{j},'file')
        disp(['     ',tbxreq.name{j} ' is not installed or not installed correctly.'])
    else
        disp(['     ',tbxreq.name{j} ' seems functional. If you hit trouble visit ',tbxreq.url{j}])
        tbxreq.installed(j)=true;
    end
end

for j=1:height(tbxreq)
    if ~tbxreq.installed(j)
        disp(['     ','Downloading and adding ',tbxreq.name{j},' (',tbxreq.dlurl{j},')'])
        try
            unzip(tbxreq.dlurl{j},userpath)
            addpath(genpath([userpath,filesep,tbxreq.foldername{j}]))
        catch
            error(['     ','Download of ',tbxreq.name{j},' failed. Visit ',tbxreq.url{j},' for help or manual download / installation.'])
        end
    end
end

disp(['     ','Adding edfsinstall toolbox to path (temp. solution prior to publication)'])
addpath(genpath('D:\Work\git\edf-git\'))
disp('Done.')

end


function access = checkonlinestatus
if ispc
    C = evalc('!ping -n 1 8.8.8.8');    
elseif isunix
    C = evalc('!ping -c 1 8.8.8.8');        
end

if ~contains(C,'unreachable')
    access=true;
else
    access=false;
end

end
