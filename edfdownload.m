function jfacsdownload(developer) %#ok<*INUSD>
%
% <strong>Syntax</strong>
%   <strong>jfacsdownload</strong>
%
% <a href="matlab: doc jfacsdownload">help for jfacsdownload</a> <- click on the link

% Download and install the jfacsdownload toolbox. This function:
%  - fetches the version-list from 'https://gitlab.com/dreem/drEEM'
%  - downloads the latest stable release
%  - extracts & installs the toolbox with the routine provided 
%     in the respective version
%
% Notes: 
%   -  Developer option: If any input to ''developer'' is provided (any number or 
%      character), the user will be ased to specify the desired version.
%       - 'nightly beta' contains the latest (not yet released) features of drEEM.
%          This version can be considered generally stable, but the code may
%          change without anouncement. 
%          View 'CHANGELOG.md' for the list of ongoing changes.
%          It is not recommended for download.
%       - 'latest released' corresponds to the first entry. This is the
%          version intended for the normal user.
%
%
% Notice:
% This mfile is be part of the fjacs toolbox.
%
%
% jfacsdownload: : Copyright (C) 2020 Urban J. Wuensch
% Chalmers University of Technology
% Sven Hultins Gata 6
% 41296 Gothenburg
% Sweden
% wuensch@chalmers.se
%% Check input

% if ~ischar(reqver)
%     error('Input to ''reqver'' must be a character, e.g. ''0.5.0''')
% end
% 
olddir=pwd;

if ~checkonlinestatus
    error('Cannot reach ''google.com''. Your computer must have internet access for this function to work.')
end
disp(' ')
disp(' ')
disp(' ')
disp('----------------------------------------------------------------------------')
disp('   jfacs toolbox - joint factorization of complex spectral data sets ')
disp(' ')
disp('               Download and installation routine                            ')
disp(' ')
disp('----------------------------------------------------------------------------')
disp(' ')
disp('Fetching version directory ...')
try
    vhist=webread("https://gitlab.com/urbanwuensch/jfacs/raw/master/versions.txt");
catch
    iaccess=checkonlinestatus;
    if ~iaccess
        error('Cannot reach ''google.com''. Your computer must have internet access for this function to work as intended.')
    elseif iaccess
        error('Cannot reach the version file, but internet connection is available. Please contact drEEM user support.')
    end
end
disp('Success. ')
cellvhist=textscan(vhist,'%s%s','Delimiter',';');
tvhist=cell2table([cellvhist{:}],'VariableNames',{'Version','URL'});

if exist('developer','var')
    disp(' ')
    disp('--- Available versions -----------')
    message=strcat(num2str((1:height(tvhist))'),repmat(" : ",height(tvhist),1),tvhist.Version);
    for n=1:size(message,1)
        disp(message(n,:))
    end
    disp('----------------------------------')
    disp(' ')
    ui=input(['Enter number of the desired release (',num2str(1:2),' ... ):  '],'s');
    reqver=tvhist.Version{str2double(ui)}; 
else
    reqver=tvhist.Version{strcmp(tvhist.Version,'latest released (same as first entry)')};
end

disp('Function does not work yet since the repo is private so far.')
% urlselected=find(strcmp(tvhist.Version,reqver));
% 
% 
% if isempty(urlselected)
%     error('Something went wrong when trying to download drEEM \n%s',...
%         '   Visit: https://gitlab.com/dreem/drEEM or: dreem.openfluor.org')
% else
%     temp=strsplit(tvhist.URL{urlselected},'/');temp=temp{end};temp=strsplit(temp,'.z');temp=temp{1};
%     disp(['Downloading toolbox (',userpath,filesep,temp,filesep,')'])   
%     clearvars temp
%     unzip(tvhist.URL{urlselected},userpath)
%     disp('Success. ')
%     folder=strsplit(tvhist.URL{urlselected},{'/'});
%     folder=folder{end};
%     folder=strsplit(folder,'.zip');
%     folder=folder{1};
%     cd([userpath,filesep,folder])
%     disp('Installing toolbox ... ')
%     disp(' ')
%     disp(' ')
% 
%     dreeminstall
%     cd(olddir)
% end

end

function access = checkonlinestatus
if ispc
    C = evalc('!ping -n 1 8.8.8.8');    
elseif isunix
    C = evalc('!ping -c 1 8.8.8.8');        
end

if ~contains(C,'unreachable')
    access=true;
else
    access=false;
end

end